export default {
  pages: [
    'pages/index/index',
    'pages/auth/index',
    'pages/user/index'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#d43c33',
    navigationBarTitleText: '容德信',
    navigationBarTextStyle: 'white'
  },
  tabBar: {
    list: [
      { pagePath: 'pages/index/index', text: '首页', iconPath: 'assets/images/tabbar/home.png', selectedIconPath: 'assets/images/tabbar/home_active.png' },
      { pagePath: 'pages/user/index', text: '我的', iconPath: 'assets/images/tabbar/user.png', selectedIconPath: 'assets/images/tabbar/user_active.png' }
    ],
    color: '#7a7a7a',
    selectedColor: '#d43c33',
    backgroundColor: '#fff',
    position: 'bottom',
    animation: true
  }
}
