import Taro from "@tarojs/taro";
/**
 * @module 系统公用方法
 */
  // 获取导航栏高度
const getNavHeight = () => {
  let menuButtonObject = Taro.getMenuButtonBoundingClientRect(); 
  const sysInfo = Taro.getSystemInfoSync();
  let statusBarHeight = sysInfo.statusBarHeight || 0; // 状态栏高度
  let menuBottonHeight = menuButtonObject.height; 
  let menuBottonTop = menuButtonObject.top; 
  let navBarHeight = statusBarHeight + menuBottonHeight + (menuBottonTop - statusBarHeight) * 2
  return navBarHeight;
}

export {
  getNavHeight
}